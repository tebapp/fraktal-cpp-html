#include <iostream>
#include <fstream>
//#include <sstream>
//#include <math.h>

using namespace std;

string createHeader();
string createEnd();
string doCarpet(int level, string data="");

int main(int , char **)
{
    ofstream file("c:\\Users\\adminteb\\index.html", ios::out);
    if(!file.is_open())
        cerr << "Plik nie może zostać utworzony, koniec programu" << endl;
    else {
        file << createHeader();
        file << doCarpet(7);
        file << createEnd();
        file.close();
    }
    return 0;
}

string createHeader() {
    string o = "";
    o.append("<!DOCTYPE html>\n<html>\n\t<head>\n\t\t<meta charset=\"utf-8\"/>\n\t\t<title>Fraktal</title>\n\t\t");
    o.append("<style>\n\t\t\t.blockSize {background: white; float: left; width: 33.3333333%; height: 33.3333333%;}\n\t\t\t.black{overflow:hidden; background: black; }\n\t\t</style>\n\t");
    o.append("</head>\n\t<body>\n\t\t");
    o.append("<div style=\"overflow:hidden; background: black; width: 100vh; height: 100vh;\">");
    return o;
}

string createEnd() {
    return "</div>\n\t\t</body>\n</html>";
}

string doCarpet(int level, string data) {
    while (level > 1) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == 1 && j == 1) data.append("<div class=\"blockSize\"></div>\n\t\t");
                else {
                    data.append("<div class=\"blockSize black\">");
                    data = doCarpet(level-1, data);
                    //data.append(doCarpet(level-1));
                    data.append("</div>\n\t\t");
                }
            }
        }
        level--;
    }

    return data;
}
